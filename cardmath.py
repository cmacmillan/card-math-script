#Run this file with python -i cardmath.py for interactive mode
import math
factorial = math.factorial
#The ollie method
def ollie(n):
	return (factorial(n)-factorialSum(n-1))/2

def factorialSum(x):
	total = 0
	for i in range(2,x+1):#x+1 because range is (inclusive,exclusive)
		total+=factorial(i)
	return total
################################

#The Chase method
def chase(n):
	return factorial(n)-calc(n)

def calc(n):
	if (n<=1):
		return 1
	return (n-1)*(calc(n-2)+(n-2)*surp(n-2))


def surp(n):
	if (n<=1):
		return 1
	return calc(n-1) + (n-1)*surp(n-1)
##################################

#The brute force method
def bruteForce(n):
	set = getAllPermutations(list(range(n)))
	counter = 0
	for i in set:
		matches = 0
		for j in range(len(i)):
			if (i[j]==j):
				matches+=1
		if (matches==0):
			counter+=1
	return counter

def getAllPermutations(set):
	superset = []
	if (len(set)<=1):
		return [set]
	for i in set:
		newset = set.copy()
		newset.remove(i)
		newset = getAllPermutations(newset)
		for j in newset:
			j.append(i)
			superset.append(j)
	return superset
#################################

print("Each of the following prints the number of ways to miss every card")
print("divide these numbers by factorial(n) to get the actual probability")
print("chase(n), my method")
print("ollie(n), ollies method")
print("bruteForce(n), brute force sampling method")
